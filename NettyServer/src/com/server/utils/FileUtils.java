/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.server.utils;

import com.server.Constants;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileUtils 
{
    /**
     * Save a file
     * @param fileName
     * @param fileHash
     * @param path 
     * @param fileContent
     */
    public static void saveFile(String fileName, String fileHash, Path path, byte[] fileContent) 
    {
        try 
        {
            int size = fileContent.length;
            Path savePath = Paths.get(path + Constants.seperator + fileName);
            
            /*if (size > 25 * 1024L * 1024L) 
            { // File too large = 25MB
              Logger.getLogger(FileUtils.class.getName()).log(Level.WARNING, "File too large!");     
              return;
            }*/
            
            if (savePath.toFile().exists()) 
            {
                Logger.getLogger(FileUtils.class.getName()).log(Level.WARNING, "File already exists!");
                //System.err.println("File already exists!");
                return;
            }
            
            Files.write(Paths.get(savePath.toString()), fileContent);
            Logger.getLogger(FileUtils.class.getName()).log(Level.INFO, "Saved file: [{0} : {1} bytes] to: {2}", new Object[]{fileName, size, savePath.toString()});
            //System.out.println("Saved file: " + "["+fileName + " : " + size + " bytes]" + " to: " + savePath.toString());
            try 
            {
                String hash = sha256sum(savePath.toFile());
                if (hash.equalsIgnoreCase(fileHash)) 
                {
                    Logger.getLogger(FileUtils.class.getName()).log(Level.INFO, "Valid sha256sum: {0} == {1}", new Object[]{hash, fileHash});
                    //System.out.println("Valid sha256sum: " + hash + " == " + fileHash);
                } 
                else 
                {
                    Logger.getLogger(FileUtils.class.getName()).log(Level.WARNING, "Error sha256sum mismatch!: {0} != {1}", new Object[]{hash, fileHash});
                    //System.err.println("Error sha256sum mismatch!: " + hash + " != " + fileHash);
                }
            }
            catch (FileNotFoundException | NoSuchAlgorithmException ex) 
            {
                Logger.getLogger(FileUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(FileUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
      * Get 256 checksum from a file
      * @param file to get sha256sum from
      * @return sha256sum of given file
      * @throws FileNotFoundException
      * @throws NoSuchAlgorithmException
      * @throws IOException 
      */
     @SuppressWarnings("empty-statement")
     public static String sha256sum(File file) throws FileNotFoundException, NoSuchAlgorithmException, IOException 
     {
        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
        FileInputStream fis = new FileInputStream(file);
  
        byte[] data = new byte[1024];
        int read = 0; 
        while ((read = fis.read(data)) != -1) 
        {
            sha256.update(data, 0, read);
        };
        byte[] hashBytes = sha256.digest();
  
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hashBytes.length; i++) 
        {
          sb.append(Integer.toString((hashBytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        String fileHash = sb.toString();
        return fileHash;
     }
}
