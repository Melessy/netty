/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import java.net.InetSocketAddress;
import java.util.logging.Level;

public class Server implements Runnable
{  
    @Override
    public void run() 
    {
      bootstrap();
    }
          void bootstrap() 
          {
          EventLoopGroup bossGroup = new NioEventLoopGroup(1); // Initialize Event loopgroup
          EventLoopGroup workerGroup = new NioEventLoopGroup(2); // Initialize worker Group
          try 
            {
             ServerBootstrap serverBootstrap = new ServerBootstrap(); // Initialize serverBootstrap
             serverBootstrap.group(bossGroup, workerGroup); // Add groups
             serverBootstrap.channel(NioServerSocketChannel.class); // Set channel mode
             serverBootstrap.localAddress(new InetSocketAddress("0.0.0.0", Constants.getServerPort())); // Define local connection, 0.0.0.0 works best
             serverBootstrap.handler(new LoggingHandler(LogLevel.INFO)); // Register LoggingHandler
             serverBootstrap.childHandler(new ChannelInitializer<SocketChannel>()  
            { // Add childHandler to initialize SocketChannel   
        /**
         * initialize SocketChannel
         * @param socketChannel
         * @throws Exception 
         */
        @Override
        protected void initChannel(SocketChannel socketChannel) throws Exception 
        {
            //socketChannel.pipeline().addLast(new ClientHandler()); // Add ClientHandler to pipeline
            socketChannel.pipeline().addLast("encoder", new LengthFieldPrepender(4)); // Encoder to write large bytes
            socketChannel.pipeline().addLast("decoder", new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4)); // Decoder to receive large bytes
            socketChannel.pipeline().addLast(new ChannelHandler()); // Add channelHandler to pipeline
            Constants.getLogger().log(Level.INFO, "Initialized SocketChannel: {0}", socketChannel);
        }
    })        .childOption(ChannelOption.SO_RCVBUF, 1024 * 1024)
              .childOption(ChannelOption.SO_SNDBUF, 1024 * 64)      
                     
              .childOption(ChannelOption.SO_KEEPALIVE, true)
              .childOption(ChannelOption.TCP_NODELAY, true)
              .childOption(ChannelOption.CONNECT_TIMEOUT_MILLIS, 25000)
              .childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT)
              .option(ChannelOption.SO_REUSEADDR, true) // SO_REUSEADDR option will allow binding to an already bound ip:port combination. This is usually used to be able to restart a server if it crashed/got killed (so while the socket is still in the TIME_WAIT state).
              .option(ChannelOption.SO_BACKLOG, 500); // The maximum queue length for incoming connection indications (a request to connect) is set to the backlog parameter. If a connection indication arrives when the queue is full, the connection is refused.
             
             ChannelFuture channelFuture = serverBootstrap.bind().sync(); // Listen for Connections
             if (channelFuture.isSuccess()) 
             {
              Constants.getLogger().log(Level.INFO, "Server is listening on port: {0}", Constants.getServerPort()); // Log Server listening
             }
              channelFuture.channel().closeFuture().sync(); // Wait until the server socket is closed
             } 
             catch(InterruptedException e) 
             {
              Constants.getLogger().log(Level.SEVERE, "Exception: {0}", e);
              //e.printStackTrace();
             } 
             finally 
             {
             workerGroup.shutdownGracefully(); // Shutdown workerGroup
             bossGroup.shutdownGracefully(); // Shutdown bossGroup
             }
    }
}
