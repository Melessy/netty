/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.server.misc;

import com.client.Client;
import com.server.Constants;
import java.time.Duration;
import java.time.Instant;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;


/**
 * Monitor Server uptime
 */
public class Uptime implements Runnable
{
    Instant start = Instant.now();
    Timer timer = new Timer();
    @Override
    public void run() 
    {
     timer.scheduleAtFixedRate(new TimerTask() 
     {
     @Override
     public void run() 
     {
     Instant finish = Instant.now();
     long timeElapsed = Duration.between(start, finish).getSeconds();
     int day = (int)TimeUnit.SECONDS.toDays(timeElapsed);        
     long hour = TimeUnit.SECONDS.toHours(timeElapsed) - (day *24);
     long minute = TimeUnit.SECONDS.toMinutes(timeElapsed) - (TimeUnit.SECONDS.toHours(timeElapsed)* 60);
     long second = TimeUnit.SECONDS.toSeconds(timeElapsed) - (TimeUnit.SECONDS.toMinutes(timeElapsed) *60);
     Constants.getLogger().log(Level.INFO, "Server uptime: Days {0} Hours {1} Minutes {2} Seconds {3}", new Object[]{day, hour, minute, second});
     Constants.getLogger().log(Level.INFO, "Connected clients: {0}", Client.getClients().size());
      }
      }, Constants.getUptimeCycle()*60*1000, Constants.getUptimeCycle()*60*1000); // Every x minutes
    }
}
