/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.server;

import com.client.Client;
import com.server.utils.FileUtils;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.CharsetUtil;
import io.netty.util.concurrent.GlobalEventExecutor;
import java.io.Serializable;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import org.apache.commons.lang.SerializationUtils;

//@Slf4j
public class ChannelHandler extends ChannelInboundHandlerAdapter 
{    
    private static final ChannelGroup channelGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    private static final AtomicInteger connectionCount = new AtomicInteger();
    
    /**
     * Serialize objects and wrap to ByteBuffer
     * @param object
     * @return byteBuffer
     */
    private ByteBuffer serializeObject(Serializable object) 
    {
         byte[] data = SerializationUtils.serialize(object);
         ByteBuffer byteBuffer = ByteBuffer.wrap(data);
         return byteBuffer;
    }
    
    /**
     * DeSerialize Objects
     * @param object
     * @return DeSerialized object
     */
    private Object deSerializeObject(Object object) 
    {
        ByteBuf byteBuf = (ByteBuf) object;
        byte[] bytes = new byte[byteBuf.readableBytes()];
        byteBuf.readBytes(bytes);
        return SerializationUtils.deserialize(bytes);
    }
    
     /**
     * Write (String) message to all clients
     * @param message 
     */
    public static void writeAll(String message) 
    {
        channelGroup.writeAndFlush(Unpooled.copiedBuffer(message, CharsetUtil.UTF_8));
    }
    
    /**
     * Write (String) message to one client
     * @param identifier, the clients name/uid/ip to write to
     * @param message 
     */
    public static void writeOne(String identifier, String message) 
    {
        for (Channel channel : channelGroup) 
        {
            Client client = Client.getClients().get(channel.id().asShortText());
            if (client.getUsername().equalsIgnoreCase(identifier) 
                    || client.getUid().equalsIgnoreCase(identifier) 
                    || client.getIp().equalsIgnoreCase(identifier)) 
            {
             channel.writeAndFlush(Unpooled.copiedBuffer(message.getBytes()));
            } 
            else 
            {
                Constants.getLogger().log(Level.WARNING, "Invalid client: {0}", identifier);
            }

        }
        }
    
    @Override
    public void channelActive(ChannelHandlerContext chc) throws Exception 
    {
        super.channelActive(chc);
        channelGroup.add(chc.channel()); // Add channel to channelGroup, Will be removed automatically upon channel Inactive
        Constants.getLogger().log(Level.INFO, "Client: {0} Connected", chc.channel().remoteAddress()); // Print Client connected
        connectionCount.incrementAndGet(); // Increase connectionCount
        Constants.getLogger().log(Level.INFO, "Active connections: {0}", connectionCount.get()); // Print Active connections
    }
    
    @Override
    public void channelInactive(ChannelHandlerContext chc) throws Exception 
    {
        super.channelInactive(chc);
        Constants.getLogger().log(Level.INFO, "Client: {0} Disconnected", chc.channel().remoteAddress()); // Print Client disconnected
        Client.getClients().remove(chc.channel().id().asShortText()); // Remove Client from map
        connectionCount.decrementAndGet(); // Decrease connectionCount
        Constants.getLogger().log(Level.INFO, "Active connections: {0}", connectionCount.get()); // Print Active connections
    }
    
    /**
     *  Read from channel and handle Client
     * @param chc
     * @param object
     * @throws Exception 
     */
    @Override
    public void channelRead(ChannelHandlerContext chc, Object object) throws Exception 
    {

        ByteBuf byteBuf = (ByteBuf) object; 
        int length = byteBuf.readableBytes();
        Constants.getLogger().log(Level.INFO, "channelRead: {0} bytes", length); // Log readable bytes received
        
        Client client = (Client) deSerializeObject(object); // DeSerialize client
        
        if (client == null) 
        {
            return;
        }
        
        client.setChannelId(chc.channel().id().asShortText()); // Set ChannelId for client
        client.setIp(((InetSocketAddress)chc.channel().remoteAddress()).getAddress().getHostAddress()); // Set ip for client
        Client.getClients().put(client.getChannelId(), client); // Put clients in HashMap
        Constants.getLogger().log(Level.INFO, "Client: {0} -> {1} -> {2} -> {3} -> {4} -> {5} -> {6} -> {7} -> {8} -> {9} -> {10}", new Object[]{client.getChannelId(), client.getUsername(), client.getUid(), client.getIp(), client.getOsName(), client.getOsType(), client.getOsVersion(), client.getJavaVersion(), client.getCountry(), client.getLanguage(), client.getMessage()}); // Log
        Constants.getLogger().log(Level.INFO, "Received Message: [{0}] @ {1} - {2}", new Object[]{client.getMessage(), client.getUsername(), client.getUid()});
        //System.out.println("Received Message: " + "["+client.getMessage()+"]" + " @ " + client.getUsername() + " - " + client.getUid());
        
        /**
         * Save inComming file
         */
           if (client.getFileName() != null && client.getFileHash() != null && client.getFileContent() != null) 
        {
           FileUtils.saveFile(client.getFileName(), client.getFileHash(), Paths.get(Constants.homeDir + Constants.seperator + "Desktop"), client.getFileContent());
        }
    }
    
    @Override
    public void channelReadComplete(ChannelHandlerContext chc) throws Exception 
    {
     super.channelReadComplete(chc);
    }
    
    @Override
    public void exceptionCaught(ChannelHandlerContext channelHandlerContext, Throwable cause) 
    {
        if (cause.getMessage().contains("java.io.EOFException")) 
        {
        Constants.getLogger().log(Level.WARNING, cause.getMessage());
        } 
        else 
        {
        cause.printStackTrace();
        }
        //cause.printStackTrace();
        channelHandlerContext.close();
    }
}
