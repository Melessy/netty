/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.server;

import com.client.Client;
import static com.server.ChannelHandler.writeAll;
import static com.server.ChannelHandler.writeOne;
import java.util.Scanner;

/**
 * CommunicationManager class to write commands to client using console
 */
public class CommunicationManager implements Runnable
{
    @Override
    public void run() 
    {
        Scanner scanner = new Scanner(System.in);
        
        while (scanner.hasNext())
        {
         //String[] command = scanner.nextLine().split(" "); // Command
        String command = scanner.nextLine();
        String[] args = command.split(" ");
        String identifier = args[1];
        
        if (command.toLowerCase().contains(".clients")) 
        {
              Client.getClients().entrySet().forEach(entry -> 
                     {
                    System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());
                    System.out.println("Name : " + entry.getValue().getUsername() + " Uid : " + entry.getValue().getUid());
                 });
        } 
        else 
        {
        if (command.contains("all")) 
        {
        writeAll(command);
        } 
        else 
        {
        writeOne(identifier, command);
    }
        }
        }
    }
}
