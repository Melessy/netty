/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.client.commands;

import com.client.ChannelHandler;
import java.io.IOException;
import java.util.Arrays;

/**
 * CommandHandler class to handle inbound commands from server
 */
public class CommandHandler 
{
    /**
     * Parse the command and execute a given command
     * @param cmd
     * @throws IOException 
     */
    public static void parseCommand(String cmd) throws IOException 
    {
       String[] args = cmd.split(" ");
       String command = args[0];
       
       /*String argument1 = args[1];
       String argument2 = args[2];
       String argument3 = args[3];*/
      
      /*if (!Arrays.toString(args).contains(Constants.username)) 
      {
          //ChannelHandler.write("Invalid username: " + c);
          System.out.println("Invalid username");
          return;
      }*/
       
      if (Arrays.toString(Commands.values()).contains(command)) 
      {
          Command c = Commands.valueOf(command).getCommand();
          ChannelHandler.writeMessage("Executing command: " + c);
          c.execute(args);
      } 
      else 
      {
          ChannelHandler.writeMessage("Invalid Command: " + command + "\n" + "Available Commands: -> " + Arrays.toString(Commands.values()));
          System.out.println("Invalid Command: " + command);
      }
      
       /* switch(Commands.valueOf(command)) 
        { // Command must be args[0]
            case TestCommand:
            TestCommand testCommand = new TestCommand();
            testCommand.execute(args);
            //System.out.println(Arrays.toString(Commands.values()));
            break;
        }*/
    }
}
