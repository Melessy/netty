/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.client;

import java.util.Locale;

public class Constants 
{
   /**
    * Operating System Info
    */
    public static String osName = System.getProperty("os.name");
    public static String osType = System.getProperty("os.arch");
    public static String osVersion = System.getProperty("os.version");
    public static String homeDir = System.getProperty("user.home");
    public static String seperator = System.getProperty("file.separator");
    public static String desktopDir = homeDir + seperator + "Desktop";
    public static String tempDir = System.getProperty("java.io.tmpdir");
    public static Locale currentLocale = Locale.getDefault();
    public static String language = currentLocale.getDisplayLanguage();
    public static String country = System.getProperty("user.country");
    public static String country_full = currentLocale.getDisplayCountry();
    public static String username = System.getProperty("user.name");
    public static String javaVersion = System.getProperty("java.version");
  //public static String language = System.getProperty("user.language");
    
   /**
    * General Info
    */
    public static final String host = "127.0.0.1";
    public static final int port = 1166;
    public static final int RECONNECT_DELAY = 15; // Re-connection delay
}
