/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.client;

import java.io.Serializable;

/**
 * Serializable interface
 */
public final class Client implements Serializable 
{
    /**
     * Variables
     */
        private static final long serialVersionUID = 1L;
        
	String username;
        String uid;
        String osName;
        String osType;
        String osVersion;
        String javaVersion;
        
        String country;
        String language;
        
        String message;
        
        String fileName;
        byte[] fileContent;
        String fileHash;
        
        /**
         * Serializable for client
         * @param username
         * @param uid
         * @param osName
         * @param osType
         * @param osVersion
         * @param javaVersion
         * @param country
         * @param language
         * @param message 
         */
         public Client(String username, 
                 String uid, 
                 String osName, 
                 String osType, 
                 String osVersion,
                 String javaVersion,
                 String country, 
                 String language,
                 String message)
        {
		this.username = username;
                this.uid = uid;
                
                this.osName = osName;
                this.osType = osType;
                this.osVersion = osVersion;
                this.javaVersion = javaVersion;
                
                this.country = country;
                this.language = language;
                
                this.message = message;
        }

        /**
         * Serializable to transfer files
         * @param username
         * @param uid
         * @param osName
         * @param osType
         * @param osVersion
         * @param javaVersion
         * @param country
         * @param language
         * @param fileName
         * @param fileContent
         * @param fileHash
         */
       public Client(String username, 
               String uid, 
               String osName, 
               String osType, 
               String osVersion, 
               String javaVersion,
               String country,
               String language,
               String fileName, 
               byte[] fileContent, 
               String fileHash)
        {
                this.username = username;
                this.uid = uid;
                
                this.osName = osName;
                this.osType = osType;
                this.osVersion = osVersion;
                this.javaVersion = javaVersion;
                
                this.country = country;
                this.language = language;
                
                this.fileName = fileName;
                this.fileContent = fileContent;
                this.fileHash = fileHash;
                
                this.message = "[File Transfer: " + fileName + " : " + fileContent.length + " bytes]";
        }
}
